<%method title>Myghty Demo Server</%method>

<div style="margin-left:20%">
<h2>
Myghty Demo Server
</h2>

<div style="margin-left: 30px;">
<a href="/source/README">README</a><br/>
<a href="/doc/">Documentation</a><br/><br/>

<a href="http://www.myghty.org/">Myghty Homepage</a><br/><br/>

<h4>Source Browser:</h4>
<div  style="margin-left: 30px;">
	<a href="/source/">Distribution Source</a><br/>
	<a href="/source/examples/">Examples</a><br/>
	<a href="/source/lib/myghty/">Myghty Source</a><br/>
</div>

<h4>Examples:</h4>
<div style="margin-left: 30px;">
<a href="/source/examples/zblog/README">ZBlog Readme</a> - a larger framework featuring MyghtyJax and SQLAlchemy<br/>
<a href="/examples/shoppingcart/store/">Shopping Cart</a> - New Implicit Module Components<br/>
<a href="/examples/myghtyjax/">MyghtyJax</a> - Asynchronous XMLHttpRequest Framework<br/>
<a href="/examples/components/">Component Calling</a><br/>
<a href="/examples/formcontrols/">Form Controls</a><br/>
<a href="/examples/formvisitor/registration.myt">Form Visitor</a> (module component calling)<br/>
<a href="/examples/template/">Template Layout</a><br/>
</div>
</div>	
</div>
