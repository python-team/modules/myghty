<span class="headertext1">Myghty Demo Store</span>

<p>
Welcome to the Myghty Shopping Cart demo.  The purpose of this demo is to provide a reasonably
functional web application which illustrates the major features of Myghty, advanced examples of
usage, as well as suggested practices to achieve a straightforward and maintainable application.
</p>

<p>
The store itself is a pretty routine application, with its inner workings being the more 
interesting thing to see.  To that end, the source of all application files is available via 
the links in the left-hand sidebar.
</p>

<p>
While all products are fictional, users are free to download the five minute microsoft paint 
images in return for a small fee :) .
</p>


<a href="<& common.myc:store_uri &>catalog/">on to the store!</a><br/>
<a href="architecture.myt">architectural introduction</a>
